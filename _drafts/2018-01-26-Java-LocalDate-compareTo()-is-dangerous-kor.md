---
layout: post
categories: articles
title:  "Java LocalDate compareTo()는 위험하다"
excerpt: "전통적인 compareTo()와 LocalDate.compareTo()의 차이점"
tags: []
date: 2018-01-26 15:30:00
modified: 2018-01-26 15:30:00
image: 
  feature:
  credit:
  creditlink:
share: true
sitemap: false
---

전통적인 compareTo(): -1, 0, 1
LocalDate.compareTo(): -x, 0, +x

https://www.tutorialspoint.com/javatime/javatime_localdate_compareto.htm

