---
layout: post
categories: articles
author: nathan
title: "Markdown 의 장점과 사용법"
excerpt: "markdown 사용법을 알아본다."
date: 2018-11-02 14:05:27
modified: 2018-11-02 14:05:27
tags: [markdown]
image:
  feature: 2018-11-01-markdown/01.png
share: true
sitemap: true
---

# Markdown
> 마크다운은 일반적인 텍스트 문서의 양식을 편집하는 문법으로 웹에서 글을 쓸 때 사용할 수 있는 텍스트-HTML 변환 도구로,2004년 John Gruber가 개발하였습니다.<br>
텍스트 형식의, 보다 직관적이고 쉬운 문법을 사용하며, 편집 도구가 없어도 언제 어디서든 글을 쓸 수 있습니다.<br>
일반적인 텍스트 문서의 구조를 만들고 HTML로 쉽게 변환할 수 있습니다.


## PHILOSOPHY
> Markdown is intended to be as `easy-to-read` and `easy-to-write` as is feasible.<br>
`Readability`, however, is emphasized above all else.<br>
A Markdown-formatted document should be publishable as-is, as plain text, without looking like it’s been marked up with tags or formatting instructions.<br>
While Markdown’s syntax has been influenced by several existing text-to-HTML filters<br>
— including Setext, atx, Textile, reStructuredText, Grutatext, and EtText—<br>
the single biggest source of inspiration for Markdown’s syntax is `the format of plain text email`.<br>
To this end, Markdown’s syntax is comprised entirely of `punctuation characters`, which punctuation characters have been carefully chosen so as to look like what they mean.<br>
E.g., asterisks around a word actually look like *emphasis*. Markdown lists look like, well, lists.<br>
Even blockquotes look like quoted passages of text, assuming you’ve ever used email.

**읽기 쉽고, 쓰기 편하게**, 이것은 마크다운(Markdown)의 철학입니다.

## 마크다운(Markdown) 장점
1. 쉽게 쓸수 있다.
2. 쉽게 읽을 수 있습니다.
3. 글에 집중할 수 있습니다.
4. 모바일 환경에 적합합니다.
5. 어디서나 동일한 결과를 보여줍니다.

 # `다른 것을 사용할 생각은 하지 말자.닥~ 마크다운 써라`

---

## Standard Markdown

### Headers

setext와 atx를 지원

```no-highlight
# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:

Alt-H1
======

Alt-H2
------
```

### Emphasis

Examples:

```no-highlight
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~
```

Become:

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

### Lists

Examples:

```no-highlight
1. First ordered list item
2. Another item
   * Unordered sub-list.
1. Actual numbers don't matter, just that it's a number
   1. Ordered sub-list
4. And another item.

* Unordered list can use asterisks
- Or minuses
+ Or pluses
```

Become:

1. First ordered list item
2. Another item
   * Unordered sub-list.
1. Actual numbers don't matter, just that it's a number
   1. Ordered sub-list
4. And another item.

* Unordered list can use asterisks
- Or minuses
+ Or pluses

### Links

There are two ways to create links, inline-style and reference-style.

    [I'm an inline-style link](https://www.google.com)

    [I'm an inline-style link with title](https://www.google.com "Google's Homepage")

    [I'm a relative reference to a repository file](LICENSE)

    [I am an absolute reference within the repository](/doc/user/markdown.md)

    [arbitrary case-insensitive reference text]: https://www.mozilla.org
    [1]: http://slashdot.org
    [link text itself]: https://www.reddit.com

### Blockquotes

> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

> 11111
>> 22222
>

### Inline HTML

You can also use raw HTML in your Markdown, and it'll mostly work pretty well.

Examples:

```no-highlight
<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. Use HTML <em>tags</em>.</dd>
</dl>
```

Become:

<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. Use HTML <em>tags</em>.</dd>
</dl>

#### Details and Summary

Content can be collapsed using HTML's tags. This is especially useful for collapsing long logs so they take up less screen space.

<p>
<details>
<summary>Click me to collapse/fold.</summary>

These details <em>will</em> remain <strong>hidden</strong> until expanded.

<pre><code>PASTE LOGS HERE, sfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkhsfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkhsfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkhsfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkhsfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkhsfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkhsfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkhsfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkhsfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkhsfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkhsfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkhsfasdfsadshdfkhaskdfhalskfhasdf,asdfkashdficjzjcakhcvlasudfakhaxcfh,zxfhasdhfaskf
kasdfhkashdfas.,dfjhasdfkhaskfhasifysadcv,xvasydhfiwfjasdf.asfdasldfuhas.df,asfasldfhasf.asdf
asdfjkhasdfhlaskdfhaslufhasakjhdfkas.,sdfhaksfkh</code></pre>

</details>
</p>

```html
<details>
<summary>Click me to collapse/fold.</summary>

These details _will_ remain **hidden** until expanded.

    PASTE LOGS HERE

</details>
```

### Horizontal Rule

Examples:

```
Three or more...

---

Hyphens

***

Asterisks

___

Underscores
```

Become:

Three or more...

---

Hyphens

***

Asterisks

___

Underscores

### Tables

Tables aren't part of the core Markdown spec, but they are part of GFM.

Example:

```
| header 1 | header 2 |
| -------- | -------- |
| cell 1   | cell 2   |
| cell 3   | cell 4   |
```

Becomes:

| header 1 | header 2 |
| -------- | -------- |
| cell 1   | cell 2   |
| cell 3   | cell 4   |

By including colons in the header row, you can align the text within that column.

Example:

```
| Left Aligned | Centered | Right Aligned | Left Aligned | Centered | Right Aligned |
| :----------- | :------: | ------------: | :----------- | :------: | ------------: |
| Cell 1       | Cell 2   | Cell 3        | Cell 4       | Cell 5   | Cell 6        |
| Cell 7       | Cell 8   | Cell 9        | Cell 10      | Cell 11  | Cell 12       |
```

Becomes:

| Left Aligned | Centered | Right Aligned | Left Aligned | Centered | Right Aligned |
| :----------- | :------: | ------------: | :----------- | :------: | ------------: |
| Cell 1       | Cell 2   | Cell 3        | Cell 4       | Cell 5   | Cell 6        |
| Cell 7       | Cell 8   | Cell 9        | Cell 10      | Cell 11  | Cell 12       |


### Superscripts / Subscripts

CommonMark and GFM currently do not support the superscript syntax ( `x^2` ) that Redcarpet does.  You can use the standard HTML syntax for superscripts and subscripts.

```
The formula for water is H<sub>2</sub>O
while the equation for the theory of relativity is E = mc<sup>2</sup>.
```

The formula for water is H<sub>2</sub>O while the equation for the theory of relativity is E = mc<sup>2</sup>.



## GitLab Flavored Markdown (GFM)

### Code and Syntax Highlighting

Blocks of code are either fenced by lines with three back-ticks <code>```</code>,
or are indented with four spaces. Only the fenced code blocks support syntax
highlighting:

```no-highlight
Inline `code` has `back-ticks around` it.
```

Inline `code` has `back-ticks around` it.

Example:

    ```javascript
    var s = "JavaScript syntax highlighting";
    alert(s);
    ```

    ```python
    def function():
        #indenting works just fine in the fenced code block
        s = "Python syntax highlighting"
        print s
    ```

    ```ruby
    require 'redcarpet'
    markdown = Redcarpet.new("Hello World!")
    puts markdown.to_html
    ```

    ```
    No language indicated, so no syntax highlighting.
    s = "There is no highlighting for this."
    But let's throw in a <b>tag</b>.
    ```

becomes:

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

```
No language indicated, so no syntax highlighting.
s = "There is no highlighting for this."
But let's throw in a <b>tag</b>.
```

### Emoji

	Sometimes you want to :monkey: around a bit and add some :star2: to your :speech_balloon:. Well we have a gift for you:

	:zap: You can use emoji anywhere GFM is supported. :v:

	You can use it to point out a :bug: or warn about :speak_no_evil: patches. And if someone improves your really :snail: code, send them some :birthday:. People will :heart: you for that.

	If you are new to this, don't be :fearful:. You can easily join the emoji :family:. All you need to do is to look up one of the supported codes.

	Consult the [Emoji Cheat Sheet](https://www.emojicopy.com) for a list of all supported emoji codes. :thumbsup:

	Most emoji are natively supported on macOS, Windows, iOS, Android and will fallback to image-based emoji where there is lack of support.

	On Linux, you can download [Noto Color Emoji](https://www.google.com/get/noto/help/emoji/) to get full native emoji support.

	Ubuntu 18.04 (like many modern Linux distros) has this font installed by default.


Sometimes you want to :monkey: around a bit and add some :star2: to your :speech_balloon:. Well we have a gift for you:

:zap: You can use emoji anywhere GFM is supported. :v:

You can use it to point out a :bug: or warn about :speak_no_evil: patches. And if someone improves your really :snail: code, send them some :birthday:. People will :heart: you for that.

If you are new to this, don't be :fearful:. You can easily join the emoji :family:. All you need to do is to look up one of the supported codes.

Consult the [Emoji Cheat Sheet](https://www.emojicopy.com) for a list of all supported emoji codes. :thumbsup:

Most emoji are natively supported on macOS, Windows, iOS, Android and will fallback to image-based emoji where there is lack of support.

On Linux, you can download [Noto Color Emoji](https://www.google.com/get/noto/help/emoji/) to get full native emoji support.

Ubuntu 18.04 (like many modern Linux distros) has this font installed by default.

### Task Lists

You can add task lists to issues, merge requests and comments. To create a task list, add a specially-formatted Markdown list, like so:

```no-highlight
- [x] Completed task
- [ ] Incomplete task
    - [ ] Sub-task 1
    - [x] Sub-task 2
    - [ ] Sub-task 3
```

- [x] Completed task
- [ ] Incomplete task
    - [ ] Sub-task 1
    - [x] Sub-task 2
    - [ ] Sub-task 3

Tasks formatted as ordered lists are supported as well:

```no-highlight
1. [x] Completed task
1. [ ] Incomplete task
    1. [ ] Sub-task 1
    1. [x] Sub-task 2
```

1. [x] Completed task
1. [ ] Incomplete task
    1. [ ] Sub-task 1
    1. [x] Sub-task 2

### Math

Example:

    This math is inline $`a^2+b^2=c^2`$.

    This is on a separate line
    ```math
    a^2+b^2=c^2
    ```

Becomes:

This math is inline $`a^2+b^2=c^2`$.

This is on a separate line
```math
a^2+b^2=c^2
```
### Mermaid

It is possible to generate diagrams and flowcharts from text using [Mermaid][mermaid].

Example:

    ```mermaid
    graph TD;
      A-->B;
      A-->C;
      B-->D;
      C-->D;
    ```

Becomes:

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

For details see the [Mermaid official page][mermaid].


## References

- This document leveraged heavily from the [Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
- The original [Markdown Syntax Guide](https://daringfireball.net/projects/markdown/syntax) at Daring Fireball is an excellent resource for a detailed explanation of standard markdown.
- The detailed specification for CommonMark can be found in the [CommonMark Spec][commonmark-spec]
- The [CommonMark Dingus](http://try.commonmark.org) is a handy tool for testing CommonMark syntax.

[^1]: This link will be broken if you see this document from the Help page or docs.gitlab.com
[^2]: This is my awesome footnote.

[markdown.md]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md
[mermaid]: https://mermaidjs.github.io/ "Mermaid website"
[rouge]: http://rouge.jneen.net/ "Rouge website"
[redcarpet]: https://github.com/vmg/redcarpet "Redcarpet website"
[katex]: https://github.com/Khan/KaTeX "KaTeX website"
[katex-subset]: https://github.com/Khan/KaTeX/wiki/Function-Support-in-KaTeX "Macros supported by KaTeX"
[asciidoctor-manual]: http://asciidoctor.org/docs/user-manual/#activating-stem-support "Asciidoctor user manual"
[commonmarker]: https://github.com/gjtorikian/commonmarker
[commonmark-spec]: https://spec.commonmark.org/current/
