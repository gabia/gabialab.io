---
layout: post
categories: project
title:  "centos 에서 firewalld 사용"
excerpt: "firewalld의 사용법을 알아본다."
author: kevin
tags: [firewall, centos]
#date: 2019-01-22 10:03:05
#modified: 2019-01-22 12:03:05
image:
  feature: 2019-01-22-centos-firewalld/endpoint.jpg
  credit:
  creditlink:
share: true
sitemap: false
---

Centos7 부터는 iptables를 사용하지 않고 firewalld를 사용한다.

# 소개
1. 요약
2. 배경

# 구성
# 설정
# 분석
# 결론


### firewalld 상태 확인

```shell
[root@elastic12 ~]# systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; disabled; vendor preset: enabled)
   Active: inactive (dead)
     Docs: man:firewalld(1)

```

# Reference
* [이 세상에 하나는 남기고 가자, firewalld vs iptables](https://blog.asamaru.net/2015/10/16/centos-7-firewalld/)
